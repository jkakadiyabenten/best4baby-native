import React, {Component} from 'react';
import { StyleSheet, View, Text } from 'react-native';
import GridView from 'react-native-super-grid';

type Props = {};
export default class App extends Component<Props> {
  render() {
	  
	  const items = [
      { name: 'MOM LIST', code: '#1abc9c' }, 
	  { name: 'SUPPORT LIST', code: '#2ecc71' },
      { name: 'VISIT CONTENT', code: '#3498db' }, 
	  { name: 'APPOINTMENT', code: '#9b59b6' },
      { name: 'TESTIMONIAL VIDEO', code: '#34495e' },
    ];
	
    return (
      <GridView
        itemDimension={200}
        items={items}
        style={styles.gridView}
        renderItem={item => (
          <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
            <Text style={styles.itemName}>{item.name}</Text>
          </View>
        )}
      />
	
    );
  }
}

const styles = StyleSheet.create({
  gridView: {
    paddingTop: 25,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
});
